	<section>
		<article id="help-us">
			<h3>Come collaborare</h3>
			<div>
			<p>
			Ci sono molti modi in cui &egrave; possibile aiutare la comunit&agrave; italiana di KDE.<br>
			Un buon punto di partenza per iniziare a seguire attivamente la redazione di KDE Italia, offrire consigli e esperienze, &egrave; iscriversi alla <a href="https://mail.kde.org/mailman/listinfo/kde-italia" target="_blank">mailing list di coordinazione della comunit&agrave; italiana</a>. Altre possibilit&agrave; di interazione sono il forum e il <a href="irc://irc.libera.chat/kde-italia" target="_blank">canale IRC</a>.<br>
			Se, inoltre, vuoi contribuire per rendere la comunit&agrave; italiana di KDE migliore, fai clic su una delle tante attivit&agrave; di seguito riportate.
			</p>
			</div>
		</article>
		<article id="contents">
			<h4>Gruppo contenuti</h4>
			<div>
			<p>
			<i><u>Ruolo:</u></i><br>
			Il Gruppo Contenuti si occupa di gestire i contenuti del sito e in generale comunicare attraverso l'utilizzo di testi e immagini.
			<br><br>
			<i>Competenze richieste:</i><br>
			<ul>
			<li>conoscenza di base del linguaggio HTML e utilizzo di CSS</li>
			<li>buona capacit&agrave; di scrittura e sintesi in lingua italiana da notizie e documenti in inglese</li>
			<li>redazione di testi e articoli</li>
			<li>possibili elementi base di art-designing</li>
			</ul>
			<a class="button" href="mailto:kde-italia@kde.org?subject=Contenuti" target="_blank">Collabora</a><br>
			</p>
			</div>
		</article>
		<article id="documentation">
			<h4>Gruppo documentazione</h4>
			<div>
			<p>
			<i><u>Ruolo:</u></i><br>
			Il Gruppo Documentazione si occupa di coordinare e realizzare la documentazione in italiano di KDE. L'impegno del redattore di contenuti verter&agrave; sulla scrittura di documenti, guide, how-to, FAQ, documenti sullo sviluppo, note di rilascio.
			<br><br>
			<i>Competenze richieste:</i><br>
			<ul>
			<li>buona padronanza della lingua inglese</li>
			<li>conoscenza approfondita delle applicazioni di KDE</li>
			</ul>
			<a class="button" href="mailto:kde-italia@kde.org?subject=Documentazione" target="_blank">Collabora</a><br>
			</p>
			</div>
		</article>
		<article id="forum">
			<h4>Gruppo forum</h4>
			<div>
			<p>
			<i><u>Ruolo:</u></i><br>
			Il Gruppo Forum si occupa della gestione e moderazione del forum di riferimento per la comunit&agrave; italiana di KDE.
			<br>
			Lo scopo del gruppo &egrave; di garantire il corretto funzionamento del forum, sia per quanto riguarda gli aspetti tecnici e organizzativi, sia per quanto riguarda l'ordine all'interno delle sezioni e la moderazione, assicurando il miglior supporto possibile per la comunit&agrave;.<br><br>
			<i>Competenze richieste:</i><br>
			<ul>
			<li>comportamento corretto e moderato con tutti, moderazione e disponibilit&agrave; al dialogo</li>
			<li>utilizzo a livello di moderazione della piattaforma forum</li>
			</ul>
			<a class="button" href="mailto:kde-italia@kde.org?subject=Forum" target="_blank">Collabora</a><br>
			</p>
			</div>
		</article>
		<article id="development">
			<h4>Gruppo sviluppo</h4>
			<div>
			<p>
			<i><u>Ruolo:</u></i><br>
			Crei applicazioni per KDE? Unisciti al Gruppo Sviluppo per promuovere la tua applicazione o per svilupparne delle altre insieme ai membri della comunit&agrave; italiana.
			<br>
			Uno degli scopi primari del gruppo &egrave; comunque quello di collaborare con il team di sviluppo di KDE.
			<br>
			Il secondo scopo &egrave; creare una discreta conoscenza delle tematiche legate allo sviluppo di KDE in modo da fornire una base solida a chi fosse interessato a conoscere i meccanismi che regolano il buon funzionamento di KDE o desiderosi di aiutare la comunit&agrave; italiana ed internazionale.
			<br>
			Altre attivit&agrave; legate a questo gruppo riguardano la segnalazione di bug, testing sull'usabilit&agrave; di desktop e applicazioni.<br><br>
			<i>Competenze richieste:</i><br>
			<ul>
			<li>C/C++, Python, Packaging, Bugfiling</li>
			</ul>
			<a class="button" href="mailto:kde-italia@kde.org?subject=Sviluppo" target="_blank">Collabora</a><br>
			</p>
			</div>
		</article>
		<article id="translate">
			<h4>gruppo traduzioni</h4>
			<div>
			<p>
			<i><u>Ruolo:</u></i><br>
			Il Gruppo Traduzione si occupa delle traduzione di articoli ufficiali e non dall'inglese all'italiano.<br><br>
			<i>Competenze richieste:</i><br>
			<ul>
			<li>buona conoscenza dell'inglese </li>
			<li>una buona conoscenza dell'italiano </li>
			<li>conoscenza degli strumenti di traduzione </li>
			</ul>
			<a class="button" href="mailto:kde-italia@kde.org?subject=Traduzioni" target="_blank">Collabora</a><br>
			</p>
			</div>
		</article>
	</section>
