	<section id="KSiteFooterLinks">
		<nav>
		<h1>Community</h1>
		<a href="https://discuss.kde.org/c/local-communities/kde-in-italy/16" target="_blank">KDE in Italy - Discuss</a>
		<a href="https://mail.kde.org/mailman/listinfo/kde-italia" target="_blank">KDE Italia mailing list</a>
		</nav>
		<nav>
		<h1>Press</h1>
		<a href="https://dot.kde.org/" target="_blank">KDE Dot News</a>
		<a href="https://planetkde.org/it/" target="_blank">KDE Italia su Planet KDE</a>
		</nav>
		<nav>
		<h1>Social</h1>
		<a href="https://www.facebook.com/groups/32240242821" target="_blank">KDE Italia su facebook</a>
		</nav>
	</section>
	
	<section id="KGlobalContributorLinks">
		<nav style="position: relative;">
			<aside class="KSocialLinks" style="position: absolute; right: 0px; top: 0px;">
			<a class="shareFacebook" target="_blank" href="http://facebook.com/kdeneon">Share on Facebook</a>
			<a class="shareGoogle" target="_blank" href="https://plus.google.com/113043070111945110583">Post on Google+</a>
			<a class="shareTwitter" target="_blank" href="https://twitter.com/KdeNeon">Share on Twitter</a>
			</aside>
		</nav>
	</section>
	
	<section id="KGlobalLegalInfo">
		<small>
		&copy; KDE Italia 2006-2016
		</small>
		<small>
		KDE<sup>&reg;</sup> ed il logo K Desktop Environment<sup>&reg;</sup>
		(<figure style="font-family: glyph">K</figure>) sono marchi registrati
		di <a href="http://ev.kde.org/" title="The KDE non-profit Organization">KDE e.V.</a>|
		<a href="http://www.kde.org/community/whatiskde/impressum.php">Legal</a>
		</small>
	</section>