<?php 
if(@$_GET['q']){$GLOBALS['getQ']=explode("/",$_GET['q']);}
if(!$getQ[0]){$getQ[0]="content";}
?>

<!DOCTYPE html>
<html class="dynamicHeightFix animate">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta name="author" content="KDE Italia" />
	<meta name="generator" content="KDE - Kate" />
	<meta name="keywords" content="KDE Italia - La comunit&agrave; italiana di riferimento per gli utenti KDE. " />
	<meta name="content" content="KDE Italia - La comunit&agrave; italiana di riferimento per gli utenti KDE. " />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta charset="UTF-8">

	<title>KDE Italia</title>
	<link href="img/logo.svg" rel="icon">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/kde.css" rel="stylesheet" type="text/css">
	<script src="js/jquery-2.js"></script>
</head>
<body class="KStandardSiteStyle">
	<header id="KGlobalHeader">
	<?php
	@include 'menu.php';
	?>
	</header>

	<main class="KLayout">
		<section id="swagIntro" class="kdeBG <?php if($getQ[0]!="content"){echo "lit";}?> overlay">
			<article>
				<img src="img/logo.svg" style="width: 200px; height: 200px; margin: 0px auto; display: block;">
				<h1>Benvenuto su <strong>KDE Italia!</strong></h1>
				<p>
					La comunit&agrave; italiana di riferimento per  gli utenti KDE.
				</p>
			</article>
		</section>

	<?php
	if(file_exists(preg_replace("/[^a-z]+/", "", $getQ[0]).'.php')){
	@include $getQ[0].'.php';
	}else{
	@include 'content.php';
	}
	?>
	</main>

	<footer id="KGlobalFooter">
	<?php
	@include 'footer.php';
	?>
	</footer>

</body>
</html>
