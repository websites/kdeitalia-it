<?php
include 'rss/lastRSS.php';

$url_flux_rss = 'https://dot.kde.org/rss.xml';
$limit      = 3;

$rss = new lastRSS;

$rss->cache_dir   = 'rss/cache';
$rss->cache_time  = 3600;
$rss->date_format = 'd/m';
$rss->CDATA       = 'content';

if ($rs = $rss->get($url_flux_rss)) 
{
  for($i=0;$i<$limit;$i++)
  {
    echo "\n".
"		<aside style=\"margin-top: 5px;\">\n".$rs['items'][$i]['pubDate']." | ".
"		<a class=\"white\" target=\"_blank\" href=\"".$rs['items'][$i]['link']."\">".$rs['items'][$i]['title']."</a>\n".
"		</aside>\n";

  }
}
else 
{
  die ('RSS non trovato');
}
?>
