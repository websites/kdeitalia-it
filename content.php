		<section>
			<img src="img/laptop.png" class="splashImage">
			
			<article>
			<?php
			@include 'rss.php';
			?>
			</article>
			
		<article id="kde-italia">
			<h1>Benvenuto su <strong>KDE Italia</strong></h1>
			<div>
			<p>
			Questo sito rappresenta l'impegno di un gruppo di appassionati nel creare una comunit&agrave; di riferimento per tutti gli utenti del software e altri prodotti della comunit&agrave; KDE in Italia.
			</p>
			<p>
			Ci sono molti modi in cui &egrave; possibile aiutare la comunit&agrave; italiana di KDE.<br>
			Un buon punto di partenza per iniziare a seguire attivamente la redazione di KDE Italia, offrire consigli e esperienze, &egrave; iscriversi alla <a href="https://mail.kde.org/mailman/listinfo/kde-italia" target="_blank">mailing list di coordinazione della comunit&agrave; italiana</a>. Altre possibilit&agrave; di interazione sono il forum e il <a href="irc://irc.libera.chat/kde-italia" target="_blank">canale IRC</a>.<br>
			Se, inoltre, vuoi contribuire per rendere la comunit&agrave; italiana di KDE migliore, fai clic su una delle attivit&agrave; presenti nella <a href="contribute#help-us">sezione dedicata</a>.
			</p>
			</div>
		</article>
		</section>
