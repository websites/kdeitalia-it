
	<nav id="KGlobalNavigation">
		<a href="content"><h1 id="KGlobalLogo">KDE</h1></a>
		<div id="responsive"></div>
		<ul id="KGlobalStaticNavigation">
			<li class="community hasMenu">Community
				<section>
					<nav class="imported">
						<article class="KCommunityPane KDisplayPane">
						<div class="KCommunityPreview">
						<h2>Benvenuti su KDE Italia!</h2>
						</div>
						</article>
						
						<nav>
						<a href="community#kde">Cos'&egrave; KDE?</a>
						<a href="community#kde-italia">KDE Italia</a>
						<a href="https://discuss.kde.org/c/local-communities/kde-in-italy/16" target="_blank">KDE in Italy - Discuss</a>
						<a href="https://mail.kde.org/mailman/listinfo/kde-italia" target="_blank">KDE Italia mailing list</a>
						<a href="https://www.facebook.com/groups/32240242821" target="_blank">KDE Italia su Facebook</a>
						<a href="https://ev.kde.org" target="_blank">KDE e.V.</a>
						</nav>
					</nav>

				</section>
			</li>
			<li class="products hasMenu">Scopri KDE
				<section>
					<nav class="imported">
						<article class="KProductPane KDisplayPane">
						<h2>Plasma Desktop</h2>
						<div class="KProductsPreview">
						<nav>
						<a target="_blank" href="https://www.kde.org/workspaces/plasmadesktop/">Il desktop Plasma</a>
						<a target="_blank" href="https://www.kde.org/screenshots/">Screenshots</a>
						<a target="_blank" href="https://community.kde.org/Distributions">Distribuzioni che usano Plasma</a>
						<a target="_blank" href="http://www.kde.org/applications/">Applicazioni</a>
<!-- 						<a href="https://www.kde.org/announcements/">Aggiornamenti</a> -->
						</nav>
						</div>
						</article>
					
						<nav>
						<a target="_blank" href="https://neon.kde.org/download">Prova</a>
						<a target="_blank" href="https://www.kde.org/download">Scarica</a>
					</nav>
				</section>
			</li>
			<li class="contribute hasMenu">Unisciti a noi
				<section><nav class="imported">
						<article class="KContributePane KDisplayPane">
						<h2>Collabora con noi!</h2>
						<h4>Unisciti alla community italiana di KDE.</h4>
						<div class="KContributeJoin">
						
						<nav>
<!-- 						<a href="contribute#community">La community</a> -->
						<a href="contribute#help-us">Come collaborare</a>
						</nav>
						</div>
						</article>
						
						<nav>
						<a href="contribute#contents">Contenuti</a>
						<a href="contribute#documentation">Documentazione</a>
						<a href="contribute#forum">Forum</a>
						<a href="contribute#development">Sviluppo</a>
						<a href="contribute#translare">Traduzioni</a>
						</nav>
						
					</nav>
				</section>
			</li>
			<li class="FAQ hasMenu">F.A.Q.
				<section><nav class="imported">
						<nav>
						<a href="faq#contribute">Come faccio a collaborare con KDE Italia?</a>
						<a href="faq#other-systems">Esiste software KDE per altri Sistemi Operativi?</a>
						<a href="faq#what-we-do">Di cosa si occupa KDE Italia? </a>
						<a href="faq#official">Questo &egrave; il sito ufficiale?</a>
						<a href="faq#gain">Guadagnate qualcosa con il sito?</a>
						</nav>
					</nav>
				</section>
			</li>
			<li class="contacts hasMenu">Contatti
				<section><nav class="imported">
						<nav>
						<a href="https://discuss.kde.org/c/local-communities/kde-in-italy/16" target="_blank">KDE in Italy - Discuss</a>
						<a href="https://mail.kde.org/mailman/listinfo/kde-italia" target="_blank">KDE Italia mailing list</a>
						<a href="https://www.facebook.com/groups/32240242821" target="_blank">KDE Italia su Facebook</a>
						<a href="https://ev.kde.org" target="_blank">KDE e.V.</a>
						</nav>
					</nav>
				</section>
			</li>
		</ul>
		<ul id="KGlobalDynamicNavigation"></ul>
	</nav>
