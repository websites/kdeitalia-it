	<section>
		<article id="contribute">
			<h1 class="faqQuestion">FAQ</h1><br>
			<h4 class="faqQuestion">Come faccio a collaborare con KDE Italia?</h4>
			<div>
			<p>
			Ci sono molti modi in cui &egrave; possibile aiutare la comunit&agrave; italiana di KDE.<br>
			Un buon punto di partenza per iniziare a seguire attivamente la redazione di KDE Italia, offrire consigli e esperienze, &egrave; iscriversi alla <a href="https://mail.kde.org/mailman/listinfo/kde-italia" target="_blank">mailing list di coordinazione della comunit&agrave; italiana</a>. Altre possibilit&agrave; di interazione sono il forum e il <a href="irc://irc.libera.chat/kde-italia" target="_blank">canale IRC</a>.<br>
			Se, inoltre, vuoi contribuire per rendere la comunit&agrave; italiana di KDE migliore, fai clic su una delle tante attivit&agrave; a disposizione nella sezione <a href="contribute">Unisciti a noi</a>.</p>
			</div>
		</article>
		<article id="other-systems">
			<h4 class="faqQuestion">Esiste software KDE per altri sistemi operativi?</h4>
			<div>
			<p>
			Il software e gli altri artefatti prodotti dalla comunit&agrave; KDE sono generalmente disponibili anche per sistemi operativi diversi da GNU/Linux, per maggiori informazioni puoi far riferimento alle pagine dei progetti:<br>
			<a href="http://freebsd.kde.org/" target="_blank">KDE per FreeBSD</a><br>
			<a href="https://community.kde.org/Windows" target="_blank">KDE per Windows&trade;</a><br>
			<a href="https://community.kde.org/Mac" target="_blank">KDE per Mac&trade;</a><br>
			<!--inoltre &egrave; possibile installare KDE anche su mobile<br>
			<a href="https://plasma-mobile.org/" target="_blank">Plasma</a><br>-->
			</p>
			</div>
		</article>
		<article id="what-we-do">
			<h4 class="faqQuestion">Di cosa si occupa KDE Italia?</h4>
			<div>
			<p>
			KDE Italia rappresenta l'impegno di un gruppo di appassionati nel creare una comunit&agrave; di riferimento per tutti gli utenti del software e altri prodotti della comunit&agrave; KDE in Italia. Se hai domande che riguardano la gestione del sito o se vuoi entrare a far parte della redazione puoi contattarci facendo uso della funzione apposita nel menu in alto.
			</p>
			</div>
		</article>
		<article id="official">
			<h4 class="faqQuestion">Questo &egrave; il sito ufficiale?</h4>
			<div>
			<p>
			Certamente. KDE Italia &egrave; noto a KDE e.V. che &egrave; l'associazione che "coordina" tutte le attivit&agrave; relative a KDE nel mondo.
			</p>
			</div>
		</article>
	</section>
