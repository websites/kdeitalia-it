	<section>
		<article id="kde">
		<h1>KDE - Panoramica della comunit&agrave; e dei progetti</h1><br>
		<div>
			<p>La comunit&agrave; KDE &egrave; formata da un numeroso gruppo aperto di sviluppatori che comprende centinaia di ingegneri del software di tutto il mondo impegnati nello sviluppo del software libero. Tutte le linee di codice di KDE sono messe a disposizione sotto una licenza open source. Ci&ograve; significa che chiunque &egrave; libero di modificare e distribuire il codice sorgente e che KDE &egrave; disponibile gratuitamente a chiunque e lo sar&agrave; sempre per chiunque.</p>


			<a id="perche_kde"></a>
			<h4>Perch&eacute; c'&egrave; bisogno di KDE Plasma?</h4>

			Il desktop tradizionale X11 manifesta, tra l'altro, i seguenti difetti:

			<ul>
			    <li> nessuna configurazione del desktop basata su finestre di dialogo semplici,</li>
			    <li> nessun sistema di aiuto unificato per le applicazioni,</li>
			    <li> nessuna struttura comune per lo sviluppo applicazioni,</li>
			    <li> nessuna struttura di creazione dei documenti,</li>
			    <li> assenza della trasparenza di rete a livello applicazione,</li>
			    <li> creazione di applicazioni X11 estremamente difficile e tediosa.</li>
			</ul>
			<a id="punto_di_vista_utente"></a>
			<h4>KDE - Il punto di vista dell'utente</h4>

			La comunit&agrave; KDE porta all'utente tra le altre cose:

			<ul>
			    <li> un "bel" desktop moderno (Plasma),</li>
			    <li> completo supporto alla trasparenza di rete e minimo bisogno di configurazione (KIO, parte di Frameworks),</li>
			    <li> un sistema integrato di aiuto che permette un accesso consistente e conveniente all'uso del desktop Plasma e delle sue applicazioni,</li>
			    <li> un aspetto coerente per tutte le applicazioni,</li>
			    <li> standardizzazione di menu, barre degli strumenti standardizzati, associazioni di tastiera, schemi di colori, ecc,</li>
			    <li> internazionalizzazione: KDE &egrave; disponibile in pi&ugrave; di 50 lingue,</li>
			    <li> finestre di dialogo coerenti per la configurazione,</li>
			    <li> un gran numero di utili applicazioni.</li>
			</ul>
<!-- TODO: Rivedere
			<a id="attuale_distribuzione"></a>
			<h4>L'attuale distribuzione di KDE</h4>

			L'attuale distribuzione ufficiale di KDE consiste dei seguenti pacchetti:


			<ul>
			    <li> KDE-Libs: varie librerie caricabili a tempo di esecuzione;</li>
			    <li> KDE-Base: i componenti base (gestore finestre, desktop, pannello, Konqueror);</li>
			    <li> KDE-Plasma-Addons: temi e applet aggiuntivi per il desktop e il pannello;</li>
			    <li> KDE-Network: Kopete, KNode, KNewsticker, Kppp, ...;</li>
			    <li> <a href="http://kontact.kde.org/">KDE-Pim</a>: KMail, KAddressbook, KOrganizer, KPilot, ...;</li>
			    <li> KDE-Graphics: applicazioni grafiche come KPDF, KDVI, KGhostview, KPaint, KFax, ...;</li>
			    <li> <a href="http://multimedia.kde.org/">KDE-Multimedia</a>: Noatun, KMidi, KSCD, ...;</li>
			    <li> <a href="http://phonon.kde.org/">Phonon</a>: l'infrastruttura multimediale di KDE, Supporta backend e sistemi operativi differenti;</li>
			    <li> <a href="http://accessibility.kde.org/">KDE-Accessibility</a>: applicazioni per migliorare l'accessibilit&agrave; per le persone disabili;</li>
			    <li> <a href="http://utils.kde.org/">KDE-Utilities</a>: KEdit, KCalc, KHexEdit, KNotes, ...;</li>
			    <li> <a href="http://edu.kde.org/">KDE-Edu</a>: programmi didattici e per bambini;</li>
			    <li> <a href="http://games.kde.org/">KDE-Games</a>: i giochi: KAsteroids, KPat, KTetris, ...;</li>
			    <li> KDE-Toys: applicazioni per lo svago;</li>
			    <li> <a href="http://kde-artists.org/">KDE-Artwork</a>: icone, stili, sfondi, salva schermo e decorazioni aggiuntive;</li>
			    <li> KDE-Admin: vari strumenti che aiutano nell'amministrazione del sistema;</li>
			    <li> KDE-SDK: script e strumenti che semplificano lo sviluppo delle applicazioni KDE;</li>
			    <li> <a href="http://koffice.org/">KOffice</a>: suite d'ufficio integrata;</li>
			    <li> <a href="http://kdevelop.org/">KDevelop</a>: ambiente di sviluppo integrato per C/C++;</li>
			    <li> KDE-Bindings: binding per vari linguaggi di programmazione (Python, Ruby, Perl, Java...);
			    <li> <a href="http://www.kdewebdev.org/">KDEWebdev</a>: strumenti di sviluppo per il Web.</li>
			</ul>

			Ci sono due pseudo-pacchetti che non sono parte dei rilasci ufficiali ma sono comunque parte del progetto KDE:

			<ul>

			    <li> <a href="http://extragear.kde.org/">KDE-Extragear</a>: Extragear &egrave; una collezione di applicazioni che sono associate al progetto KDE, ma non sono parte della distribuzione principale di KDE per varie ragioni. Questa gli d&agrave; maggiore visibilit&agrave; verso i traduttori e chi si occupa di scrivere documentazione. Nello specifico ci sono applicazioni stabili che semplicemente seguono le loro date di rilascio non legate a quelle dei pacchetti principali di KDE. Tra queste applicazioni, per esempio, troviamo K3b.</li>
			    <li> KDE-Playground: &egrave; abbastanza simile ad Extragear, ma questo pseudo-pacchetto contiene anche software che non &egrave; parte della distribuzione principale di KDE anche se fa parte del progetto.</li>
			</ul>

			Infine, ci sono centinaia di applicazioni di KDE molto buone che non sono parte dei rilasci di KDE e che nemmeno fanno parte in modo ufficiale del progetto KDE. Queste applicazioni possono essere ricercate nella base dati centrale su <a href="http://www.kde-apps.org">www.kde-apps.org</a>, classificate secondo il tipo di applicazione (rete, telefonia, ...).
			<a id="storia_kde"></a>
			<h4>Un po' di storia di KDE</h4>

			<ul>
			    <li> KDE &egrave; stato fondato nell'ottobre 1996. Il suo annuncio &egrave; datato 14 ottobre 1996.</li>
			    <li> 15 agosto 1997: KDE-ONE in Arnsberg, Germania, 15 partecipanti</li>
			    <li> Dicembre 1997: si fonda il KDE e.V.i.G. per proteggere i membri principali da responsabilit&agrave; legali e finanziarie</li>
			    <li> 8 aprile 1998: si annuncia la KDE Free Qt Foundation</li>
			    <li> Beta 1 20 ottobre 1997 - Beta 2 23 novembre 1997 - Beta3 1 febbraio 1998 - Beta4 19 aprile 1998</li>
			    <li> 1.0 rilasciata il 12 luglio 1998</li>
			    <li> 1.1 rilasciata il 6 febbraio 1999</li>
			    <li> 1.1.1 rilasciata il 5 maggio 1999</li>
			    <li> 1.1.2 rilasciata il 13 settembre 1999</li>
			    <li> KDE-Two meeting in Erlangen, Oct 7-10 1999</li>
			    <li> 1.89 rilasciata il 15 dicembre, 1999</li>
			    <li> 1.90 (KDE2 beta1) 12 maggio 2000 - 1.91 (KDE2 beta2) 14 giugno 2000 - 1.92 (KDE2 beta3) 31 luglio 2000</li>
			    <li> KDE-Three Beta meeting in Trysil (Norway), 9-19 luglio 2000</li>
			    <li> 1.93 (KDE2 beta4) rilasciata il 23 agosto 2000</li>
			    <li> Qt diventa GPL il 4 settembre 2000</li>
			    <li> 1.94 (KDE2 beta5) rilasciata il 15 settembre 2000</li>
			    <li> 2.0 Release Candidate rilasciata il 10 ottobre 2000</li>
			    <li> 3.0 rilasciata il 3 aprile 2002</li>
			    <li> 4.0 Alpha 1 rilasciata l'11 maggio 2007</li>
			</ul>
			
			Per una panoramica dettagliata ed aggiornata di tutti i rilasci &egrave; possibile visitare <a href="https://community.kde.org/Schedules" target="_blank">https://community.kde.org/Schedules</a>

			<a id="curiosita_kde"></a>
			<h4>Fatti e cifre su KDE</h4>

			KDE &egrave; un grosso progetto. Sebbene sia molto difficile quantificare cosa significhi esattamente, si pu&ograve; dire che:

			<ul>
			    <li> il deposito SVN del codice sorgente di KDE mantiene correntemente oltre i 4.0 milioni di linee di codice. (Per fare un confronto, il kernel Linux versione 2.5.71 consiste di circa 3.7 milioni di linee di codice.)</li>
			    <li> Oltre 1900 persone che contribuiscono nello sviluppo di KDE.</li>
			    <li> Solo la squadra dei traduttori consiste di circa 300 persone.</li>
			    <li> 11014 depositi in CVS sono stati fatti durante il Maggio 2002.</li>
			    <li> KDE ha pi&ugrave; di 20 mirror WWW ufficiali in 14 paesi.</li>
			    <li> KDE ha pi&ugrave; di 130 mirror FTP ufficiali in oltre 40 nazioni.</li>
			</ul>
-->

		</div>
		</article>
<!--
		<article id="gestione">
		
		<h1>Gestione del progetto</h1><br>
		<div>
		    <p>In circa 6 mesi di sviluppo di KDE, il progetto inizi&ograve; a crescere davvero tanto e inizi&ograve; ad attrarre l'interesse di un gran numero di persone. Sembrava che ci fossero discussioni senza fine e un conflitto costante cominci&ograve; ad ostacolare lo sviluppo. Per mantenere un processo decisionale salutare, divenne necessario per il gruppo principale degli sviluppatori aprire canali di comunicazione interna (tramite mailing list) e limitare l'accesso in scrittura su kde-core-devel, la mailing list degli sviluppatori principali nata appunto per gli sviluppatori effettivi, piuttosto che per spettatori ostinati nelle proprie opinioni. L'accesso in lettura a kde-core-devel &egrave; aperto a chiunque.</p>
		    <p>L'appartenenza al gruppo principale "The KDE Core Team" si basa esclusivamente sul merito. Chiunque pu&ograve; diventare un membro del gruppo principale, ma lo specifico candidato deve essersi distinto per contributi rilevanti e per la sua dedizione al progetto KDE per un periodo di tempo considerevole. Il gruppo principale di KDE decide sulla direzione generale del progetto KDE e gestisce il piano di rilascio. Contrariamente allo sviluppo di altri progetti di software libero, tra cui il pi&ugrave; noto &egrave; kernel Linux, non si ha un singolo "benevolo dittatore" che decide sulle questioni importanti. Piuttosto, il gruppo principale di KDE (che consiste di circa 20 sviluppatori) prende decisioni sulla base dei risultati di procedure di voto democratico.</p>
		    <p>Gli sviluppatori e gli utenti KDE comunicano principalmente tramite un certo numero di <a href="http://www.kde.org/mailinglists/">mailing list</a>: kde, kde-core-devel, kde-devel, kde-user, kde-commits, e altre.</p>	
		  </div>
		</article>
-->
	</section>
