	<section>
		<article id="kde">
		<h1>Cos'&egrave; KDE?</h1>
		<div>
			<h4>Descrizione generale</h4>
			<p>KDE &egrave; una rete mondiale di ingegneri del software, artisti, scrittori, traduttori e assistenti che si dedicano allo sviluppo di software libero. Questa comunità ha creato centinaia di applicazioni libere come parte di Frameworks, Workspaces e Applications di KDE.</p>
                        <p>KDE è un'impresa cooperativa in cui nessuna singola entità controlla il lavoro o i prodotti di KDE a discapito altrui.
                        Chiunque è invitato a unirsi e contribuire a KDE, tu incluso.</p>
			<h4>KDE - L'ambiente desktop Plasma</h4>
			<p>Uno dei progetti centrali della comunit&agrave; KDE &egrave; il desktop Plasma. Con Plasma &egrave; ora possibile usare in maniera semplice un ambiente desktop moderno per UNIX. Assieme a una libera implementazione di UNIX come GNU/Linux, Plasma costituisce una piattaforma di elaborazione aperta e completamente libera disponibile a chiunque senza pagare e provvista dei codici sorgenti per chi vuole apportare modifiche. Certo, possibilit&agrave; per i miglioramenti ce ne saranno sempre, per&ograve; nel fattempo crediamo di aver fatto nascere una possibile alternativa ad alcuni dei pi&ugrave; gettonati sistemi operativi/desktop commerciali attualmente disponibili. Speriamo che la combinazione UNIX/Plasma alla fine porter&agrave; allo stesso modo di usare il computer in modo aperto, affidabile, stabile e libero da monopoli sia per gli utenti medi di computer che per scienziati e professinisti informatici di tutto il mondo che man mano ne hanno beneficiato per anni.</p>
			<h4>KDE - La struttura di sviluppo delle applicazioni</h4>
			<p>Creare applicazioni multipiattaforma &egrave; stato storicamente un processo estremamente tedioso e difficile. KDE riconosce il fatto che una piattaforma di elaborazione &egrave; tanto buona quante pi&ugrave; applicazioni di prima classe vi sono per quella particolare piattaforma. In vista di ci&ograve; la comunità KDE ha sviluppato un'infrastruttura di prim'ordine per le applicazioni con documenti composti, implementando gli ultimi progressi della tecnologia delle infrastrutture e ponendosi quindi in diretta competizione con i pi&ugrave; popolari ambienti di sviluppo. Le librerie che compongono KDE Frameworks sono dei mattoncini che estendono le gi&agrave; efficienti librerie Qt e permettono di creare in modo semplice applicazioni multipiattaforma</p>
			<h4>In pi&ugrave;...</h4>
			<p>Le seguenti pagine intendono fornire orientamento e informazioi su KDE per quelli che cercano di avvicinarsi per la prima volta a KDE e al progetto stesso. Crediamo di aver coperto una buona parte di questioni e speriamo che questa descrizione vi aiuter&agrave; a comprendere KDE e il suo siluppo. Le domande ricorrenti su KDE offrono ulteriori risposte ad alcuni delle pi&ugrave; comuni risposte sull'argomento.</p>
			<p>Sono disponibili qui alcune presentazioni su KDE, sul suo sviluppo e le strutture di KDE.</p>
			<ul>
			<li><a href="kde">La comunità KDE</a>
			<ul>
			<li><a href="kde#perche_kde">Perch&eacute; serve KDE?</a></li>
			<li><a href="kde#punto_di_vista_utente">KDE - Il punto di vista dell'utente</li>
			<li><a href="kde#attuale_distribuzione">L'attuale distribuzione di KDE</li>
			<li><a href="kde#storia_kde">Un po' di storia di KDE</li>
			<li><a href="kde#curiosita_kde">Fatti e cifre su KDE</li>
			</ul></li>
			<li><a href="kde#gestione">La gestione del progetto KDE</a></li>
			<li><a href="kde#modello">Il modello di sviluppo di KDE</a></li>
			</ul>
		</div>
		</article>

		<article id="kde-italia">
			<h1>La community italiana</h1>
			<div>
			<p>
			Questo sito rappresenta l'impegno di un gruppo di appassionati nel creare una comunit&agrave; di riferimento per tutti gli utenti del software e altri prodotti della comunit&agrave; KDE in Italia.
			</p>
			<p>
			Ci sono molti modi in cui &egrave; possibile aiutare la comunit&agrave; italiana di KDE.<br>
			Un buon punto di partenza per iniziare a seguire attivamente la redazione di KDE Italia, offrire consigli e esperienze, &egrave; iscriversi alla <a href="https://mail.kde.org/mailman/listinfo/kde-italia" target="_blank">mailing list di coordinazione della comunit&agrave; italiana</a>. Altre possibilit&agrave; di interazione sono il forum e il <a href="irc://irc.libera.chat/kde-italia" target="_blank">canale IRC</a>.<br>
			Se, inoltre, vuoi contribuire per rendere la comunit&agrave; italiana di KDE migliore, fai clic su una delle attivit&agrave; presenti nella <a href="contribute#help-us">sezione dedicata</a>.
			</p>
			</div>
		</article>

	</section>
